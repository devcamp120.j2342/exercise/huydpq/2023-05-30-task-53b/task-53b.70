import java.sql.Date;

import models.Customer;
import models.Visit;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("huy");
        Customer customer2 = new Customer("trâm");
        System.out.println("Customer 1");
        System.out.println(customer1);
        System.out.println("Customer 2");
        System.out.println(customer2);

        Visit visit1 = new Visit(customer1, new Date(0));
        Visit visit2 = new Visit(customer2, new Date(0));
        System.out.println("Visit 1");
        System.out.println(visit1);
        System.out.println("Visit 2");
        System.out.println(visit2);

        visit1.setServiceExpence(1.0);
        visit1.setProductExpence(2.0);
        
        System.out.println( visit1.getTotalExpence());


    }
}
