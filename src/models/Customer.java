package models;

public class Customer {
    private String name;
    private Boolean member;
    private String membertype;

    public Customer (String name){
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Boolean getMember() {
        return member;
    }
    public void setMember(Boolean member) {
        this.member = member;
    }
    public String getMembertype() {
        return membertype;
    }
    public void setMembertype(String membertype) {
        this.membertype = membertype;
    }
    @Override
    public String toString() {
        return "Customer [name=" + name +"]";
    }
    
}
