package models;

import java.sql.Date;

public class Visit {
    private Customer customer;
    private Date date;
    private double serviceExpence;
    private double productExpence;

    public Visit(Customer name, Date date) {
        this.customer = name;
        this.date = date;
    }

    public String getName() {
        return customer.getName();
    }

    public double getServiceExpence() {
        return serviceExpence;
    }

    public void setServiceExpence(double serviceExpence) {
        this.serviceExpence = serviceExpence;
    }

    public double getProductExpence() {
        return productExpence;
    }

    public void setProductExpence(double productExpence) {
        this.productExpence = productExpence;
    }

    public double getTotalExpence() {
        double toTalExpence = serviceExpence + productExpence;
        return toTalExpence;
    }

    @Override
    public String toString() {
        return "Visit [" + this.customer + ", date=" + this.date + ", serviceExpence=" + this.serviceExpence
                + ", productExpence=" + this.productExpence + "]";
    }

}
